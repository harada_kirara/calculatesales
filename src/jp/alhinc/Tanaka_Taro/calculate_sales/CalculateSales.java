package jp.alhinc.Tanaka_Taro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	/* 支店定義ファイル名 */
	private static final String BRANCH_FILE_NAME = "branch.lst";

	/* 支店別集計ファイル名 */
	private static final String BRANCH_OUT_FILE_NAME = "branch.out";

	/* エラーメッセージ：予期せぬエラーが発生しました */
	private static final String ERR_MSG_E001 = "予期せぬエラーが発生しました";

	/* エラーメッセージ：支店定義ファイルが存在しません */
	private static final String ERR_MSG_E002 = "支店定義ファイルが存在しません";

	/* エラーメッセージ：支店定義ファイルのフォーマットが不正です */
	private static final String ERR_MSG_E003 = "支店定義ファイルのフォーマットが不正です";

	public static void main(String[] args) {//←ファイル名が"String"配列.つまり、[]複数の情報が入ってる
		//コマンドライン引数のチェック
		if (args.length != 1) {
			System.out.println(ERR_MSG_E001);
			return;
		}
		// 支店コード、支店名を設定するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コード、売上金額を設定するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!brachFileRead(args[0], BRANCH_FILE_NAME, branchNames, branchSales)) {
			return;
		}

		// ここから集計処理を作成してください。
			File[]files = new File(args[0]).listFiles();//←コマンドライン引数は"args"にある
			
			List<File> rcdFiles = new ArrayList<>();// impoat要！
			
			for(int i = 0; i < files.length ; i++) {
				files[i].getName();//名前だけとってくるよ
			
			if (files[i].getName().matches("^[0-9]{8}+rcd$")) {
				rcdFiles.add(files[i]);
				}	
			}
			//rcdファイルの数だけループ
			 for(int i = 0; i < rcdFiles.size() ; i++) {
				 //rcdFilesは.size(配列はlength)
			 //そのなかでリストを作る
				 List<String> rcdFilesA = new ArrayList<String>();
				 FileReader fr = new FileReader(rcdFilesA.get(i));
				 BufferedReader br = null;
				 br = new BufferedReader(fr);
				
				String line;
				while((line = br.readLine()) != null) {
					rcdFilesA.add(files[i]);
				}
			 }
			/*long fileSale = Long.parseLong("branchSales");
			
			Long saleAmont = 
			}*/
			
		// 支店定義ファイル出力処理
		if(!fileWrite(args[0], BRANCH_OUT_FILE_NAME, branchNames, branchSales)) {
			return;
		}

	}
	/* 支店定義ファイル読み込み処理 */
	private static boolean brachFileRead(String filePath, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(filePath, fileName);
			// ファイルが存在しない場合
			if(!file.exists()) {
				System.out.println(ERR_MSG_E002);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				// 支店コードと売上を分割する
				String items[] = line.split(",");
				// 支店定義ファイルの行数と支店コードのチェック
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println(ERR_MSG_E003);
					return false;
				}
				// 支店コードをKeyに支店名を設定する
				branchNames.put(items[0], items[1]);
				// 支店コードをKeyに売上金額を設定する
				branchSales.put(items[0], (long)0);
			}
		} catch(IOException e) {
			System.out.println(ERR_MSG_E001);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(ERR_MSG_E001);
					return false;
				}
			}
		}
		return true;
	}

	/* 支店定義ファイル出力処理 */
	private static boolean fileWrite(String filePath, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			// 支店コードの件数分集計ファイルに書き込み処理を行う
			for(String key: branchNames.keySet()) {
				// 支店コード + 支店名 + 売上金額を1行の文字列に連結
				String line = key + "," + branchNames.get(key) + "," + branchSales.get(key);
				// 一行書き込む
				bw.write(line);
				// 改行
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(ERR_MSG_E001);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(ERR_MSG_E001);
					return false;
				}

			}
		}
		return true;
	}

}
